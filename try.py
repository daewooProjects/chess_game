import itertools
from itertools import starmap

WHITE = "white"
BLACK = "black"

class Game:
    def __init__(self):
        self.playersturn = BLACK
        self.message = "this is where prompts will go"
        self.gameboard = {}
        self.placePiece()
        print("chess program. enter moves in algebraic notation separated by space")
        self.main()

    def placePiece(self):
        for i in range(0, 8):
            self.gameboard[(i, 1)] = Pawn(WHITE, uniDict[WHITE][Pawn], 1)
            self.gameboard[(i, 6)] = Pawn(BLACK, uniDict[BLACK][Pawn], -1)

        placers = [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook]

        for i in range(0, 8):
            self.gameboard[(i, 0)] = placers[i](WHITE, uniDict[WHITE][placers[i]])
            self.gameboard[(7-i), 7] = placers[i](BLACK, uniDict[BLACK][placers[i]])

    def main(self):
        while True:
            self.printBoard()
            print(self.playersturn)
            print(self.message)
            self.message = ""
            startpos,endpos = self.parseInput()
            try:
                target = self.gameboard[startpos]
            except:
                print("could not find piece; index probably out of range")
                target = None

            if target:
                print("found", str(target))
                if target.color != self.playersturn:
                    print("you aren't allowed to move that piece this turn")
                    continue
                if target.isValid(startpos, endpos, target.color, self.gameboard):
                    self.message = "that is a valid move"
                    self.gameboard[endpos] = self.gameboard[startpos]
                    del self.gameboard[startpos]
                    if self.isCheck():
                        if self.playersturn == BLACK:
                            self.playersturn = WHITE
                        else:
                            self.playersturn = BLACK
                    else:

                        break
                else:
                    self.message = "invalid move " + str(target.availableMoves(startpos[0], startpos[1], self.gameboard))
                    print(target.availableMoves(startpos[0], startpos[1], self.gameboard))
            else:
                self.message = "There in no piece in that space"

    def isCheck(self):
        king = King
        kingDict = {}
        pieceDict = {BLACK:[], WHITE:[]}
        for position, piece in self.gameboard.items():
            if type(piece) == King:
                kingDict[piece.color] = position
            print(piece)
            pieceDict[piece.color].append((piece, position))
        print(kingDict)
        if (WHITE in kingDict) and (BLACK in kingDict):
            if self.canSeeKing(kingDict[WHITE], pieceDict[BLACK]):
                print("WHITE is in check")
            if self.canSeeKing(kingDict[BLACK], pieceDict[WHITE]):
                print("BLACK in in check")
            return True
        else:
            if WHITE in kingDict: print("WHITE player has won")
            else:
                self.printBoard()
                print("BLACK player has won")
            return False
    def canSeeKing(self, kingpos, pieceLists):
        for piece, position in pieceLists:
            if piece.isValid(position, kingpos, piece.color, self.gameboard):
                return  True

    def printBoard(self):
        print("   1 | 2 | 3 | 4  | 5 | 6 | 7 | 8 |")
        for i in range(0, 8):
            print("-" * 32)
            print(chr(i + 97), end="|")
            for j in range(0, 8):
                item = self.gameboard.get((i, j), " ")
                print(str(item) + ' |', end=" ")
            print()
        print("-" * 32)

    def parseInput(self):
        try:
            a,b = input().split()
            a = ((ord(a[0]) - 97), int(a[1]) - 1)
            b = (ord(b[0]) - 97, int(b[1]) - 1)
            print(a, b)
            return (a, b)
        except:
            print("error decoding input. please try again")
            return ((-1, -1), (-1, -1))

chessCardinals = [(1,0),(0,1),(-1,0),(0,-1)]
chessDiagonals = [(1,1),(-1,1),(1,-1),(-1,-1)]
class Piece:
    def __init__(self, color, name):
        self.name = name
        self.color = color
        self.position = None

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name

    def isValid(self, startpos , endpos, Color, gameboard):

        if endpos in self.availableMoves(startpos[0], startpos[1], gameboard, Color):
            return True
        return False

    def availableMoves(self, x, y, gameboard):
        print("ERROR: no movement for base class")

    def noConflict(self, gameboard, initialColor, x, y):
        "checks if a single position poses no conflict to the rules of chess"
        if self.isInBounds(x, y) and (((x, y) not in gameboard) or gameboard[(x, y)].color != initialColor):
            return True
        return False

    def isInBounds(self, x, y):
        "checks if a position is on the board"
        if x >= 0 and x < 8 and y >= 0 and y < 8:
            return True
        return False

    def AdNauseam(self, x, y, gameboar, Color , intervals):
        answer = []
        for xint, yint in intervals:
            xtemp,ytemp = x+xint,y+yint
            #print((xint, yint), (xtemp, ytemp))
            while self.isInBounds(xtemp, ytemp):
             #   print(str((xtemp, ytemp))+" - is in bound")

                target = gameboar.get((xtemp, ytemp), None)
              #  print(target)
                if target is None: answer.append((xtemp, ytemp))
                elif target.color != Color:
                    answer.append((xtemp, ytemp))
                    break
                else:
                    break

                xtemp, ytemp = xtemp + xint, ytemp + yint
        #print(answer)
        return answer


def knightsList(x, y, int1, int2):
    array = [(x+int1, y+int2), (x+int1, y-int2), (x-int1, y+int2), (x-int1, y-int2), (x+int2, y+int1), (x+int2, y-int1), (x-int2, y+int1), (x-int2, y-int1)]
    return array
def kingList(x, y):
    array = [(x+1, y+1), (x+1, y-1), (x-1, y+1), (x-1, y-1), (x, y+1), (x, y-1), (x+1, y), (x-1, y)]
    return array

class Pawn(Piece):
    def __init__(self, color, name, direction):
        self.color = color
        self.name = name
        self.direction = direction
    def availableMoves(self, x, y, gameboard, Color = None):
        if Color is None: Color = self.color
        answer = []

        if (x+1, y+self.direction) in gameboard and self.noConflict(gameboard, Color, x+1, y+self.direction): answer.append((x+1, y+self.direction))
        if (x-1, y + self.direction) in gameboard and self.noConflict(gameboard, Color, x - 1, y + self.direction): answer.append((x - 1, y + self.direction))
        if (x, y + self.direction) not in gameboard and Color == self.color: answer.append((x, y + self.direction))
        return answer

class Knight(Piece):
    def availableMoves(self, x, y, gameboard, Color = None):
        if Color is None: Color = self.color
        return [(xx, yy) for xx,yy in knightsList(x, y, 2, 1) if self.noConflict(gameboard, Color, xx, yy)]


class Rook(Piece):
    def availableMoves(self, x, y, gameboard, Color = None):
        if Color is None: Color = self.color
        return self.AdNauseam(x, y, gameboard, Color, chessCardinals)

class King(Piece):
    def availableMoves(self, x, y, gameboard, Color = None):
        if Color is None: Color = self.color
        return [(xx, yy) for xx,yy in kingList(x, y) if self.noConflict(gameboard, Color, xx, yy)]

class Bishop(Piece):
    def availableMoves(self, x, y, gameboard, Color = None):
        if Color is None: Color = self.color
        return  self.AdNauseam(x, y, gameboard, Color, chessDiagonals)

class Queen(Piece):
    def availableMoves(self, x, y, gameboard, Color = None):
        if Color is None: Color = self.color
        return  self.AdNauseam(x, y, gameboard, Color, chessCardinals+chessDiagonals)

uniDict = {WHITE : {Pawn : "♙", Rook : "♖", Knight : "♘", Bishop : "♗", King : "♔", Queen : "♕" }, BLACK : {Pawn : "♟", Rook : "♜", Knight : "♞", Bishop : "♝", King : "♚", Queen : "♛" }}


Game()